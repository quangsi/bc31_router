import axios from "axios";
import { TOKEN_CYBERSOFT } from "./configURL";

export let movieSerivce = {
  getMovieList: () => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
};
