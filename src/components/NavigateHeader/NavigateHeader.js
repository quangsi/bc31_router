import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class NavigateHeader extends Component {
  render() {
    return (
      <div className=" h-20 flex justify-between bg-blue-400 items-center text-2xl px-10 text-white">
        <NavLink activeClassName="text-red-500" to="/" exact>
          Home
        </NavLink>
        <NavLink activeClassName="text-red-500" to="/movies">
          Movie List
        </NavLink>
        <NavLink activeClassName="text-red-500" to="/demo-hook">
          Demo Hook
        </NavLink>
        <NavLink activeClassName="text-red-500" to="/login">
          Login
        </NavLink>
      </div>
    );
  }
}
