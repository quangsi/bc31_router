import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import LoginPage from "./pages/LoginPage/LoginPage";
import NavigateHeader from "./components/NavigateHeader/NavigateHeader";
import DetailMoviePage from "./pages/DetailMoviePage/DetailMoviePage";
import MoviesPage from "./pages/MoviesPage/MoviesPage";
import DemoHookPage from "./pages/DemoHookPage/DemoHookPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <NavigateHeader />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/login" component={LoginPage} />
          <Route
            path="/movies"
            render={() => {
              return (
                <MoviesPage
                // this.props
                />
              );
            }}
          />

          <Route path={`/detail/:id`} component={DetailMoviePage} />
          <Route path={`/demo-hook`} component={DemoHookPage} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
