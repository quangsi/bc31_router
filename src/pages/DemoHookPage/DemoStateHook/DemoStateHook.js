import React from "react";
import { useState } from "react";

export default function DemoStateHook() {
  const [like, setLike] = useState(10);
  const [share, setShare] = useState(0);

  const handlePlusLike = () => {
    console.log("yes");
    setLike(like + 1);
  };
  const handlePlusShare = () => {
    setShare("Hello");
  };
  // console.log("yes plus like");
  return (
    <div className=" p-20 text-center">
      <h2>DemoStateHook</h2>

      <div>
        <span className="text-2xl mx-10 ">{like}</span>
        <button
          onClick={handlePlusLike}
          className="rounded px-5 py-3 bg-red-500 text-white"
        >
          Plus like
        </button>
      </div>

      <div>
        <span className="text-2xl mx-10 ">{share}</span>
        <button
          onClick={handlePlusShare}
          className="rounded px-5 py-3 bg-blue-500 text-white"
        >
          Plus share
        </button>
      </div>
    </div>
  );
}
