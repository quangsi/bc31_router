import React, { memo } from "react";

// memo ~ pureComponnet ~ xử lý component
function DemoHeaderHook({ like, handlePlusLike }) {
  console.log("header render");
  return (
    <div className=" p-10 bg-gray-700 text-white">
      <p>DemoHeaderHook</p>
      <span>Like: {like}</span>

      <button
        onClick={handlePlusLike}
        className="px-5 py-2 rounded bg-white text-black"
      >
        handlePlusLike
      </button>
    </div>
  );
}

export default memo(DemoHeaderHook);
