import React, { useMemo, useState } from "react";
import { useCallback } from "react";
import { useEffect } from "react";
import DemoHeaderHook from "./DemoHeaderHook";

export default function DemoLifeCycleHook() {
  const [like, setLike] = useState(1);
  const [share, setShare] = useState(1);
  const [scoreArr, setScoreArr] = useState([1, 2, 3, 4]);
  useEffect(() => {
    console.log("yes useEffect");
  }, [like, share]);
  // useCallback ghi nhớ function
  const handlePlusLike = useCallback(() => {
    setLike(like + 1);
  }, [like]);
  // useMemo : ghi nhớ kết quả tính toán
  let totalScore = useMemo(() => {
    return scoreArr.reduce((pre, current) => {
      console.log("usememo");
      return pre + current;
    }, 0);
  }, [scoreArr.length]);

  // let totalScore = scoreArr.reduce((pre, current) => {
  //   console.log("usememo");
  //   return pre + current;
  // }, 0);
  console.log("totalScore: ", totalScore);
  return (
    <div className="text-center py-20">
      <h2>DemoLifeCycleHook</h2>

      <DemoHeaderHook handlePlusLike={handlePlusLike} like={like} />

      <div>
        <span className="">{like}</span>
        <button
          onClick={() => {
            setLike(like + 1);
          }}
          className=" px-5 py-2 rounded bg-red-400"
        >
          Plus like
        </button>
      </div>

      <div>
        <span className="">{share}</span>
        <button
          onClick={() => {
            setShare(share + 1);
          }}
          className=" px-5 py-2 rounded bg-blue-400"
        >
          Plus share
        </button>
      </div>
    </div>
  );
}
