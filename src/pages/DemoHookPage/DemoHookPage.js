import React from "react";
import DemoLifeCycleHook from "./DemoLifeCycleHook/DemoLifeCycleHook";
import DemoStateHook from "./DemoStateHook/DemoStateHook";

export default function DemoHookPage() {
  return (
    <div>
      <DemoStateHook />

      <div className="h-2 bg-black my-5"></div>

      <DemoLifeCycleHook />

      <div className="h-2 bg-black my-5"></div>
    </div>
  );
}
