import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class MovieItem extends Component {
  render() {
    let { phim } = this.props;
    console.log("phim : ", phim);
    return (
      <NavLink to={`/detail/${phim.maPhim}`}>
        <div className="rounded p-3 shadow-lg transition hover:shadow-orange-600 hover:shadow-2xl">
          <img
            src={phim.hinhAnh}
            alt=""
            className="w-full h-60 object-cover object-top"
          />
          <p className="text-blue-400 py-3 text-center">{phim.tenPhim}</p>
        </div>
      </NavLink>
    );
  }
}
