import React, { Component } from "react";
import { movieSerivce } from "../../services/movieService";
import MovieItem from "./MovieItem/MovieItem";

export default class MoviesPage extends Component {
  state = {
    danhSachPhim: [],
  };
  componentDidMount() {
    movieSerivce
      .getMovieList()
      .then((res) => {
        console.log(res);
        this.setState({
          danhSachPhim: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderMovies = () => {
    return this.state.danhSachPhim.map((phim) => {
      return <MovieItem phim={phim} key={phim.maPhim} />;
    });
  };
  render() {
    return (
      <div>
        <div className="grid grid-cols-4 gap-5 p-10">{this.renderMovies()}</div>
      </div>
    );
  }
}
